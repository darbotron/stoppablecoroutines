# Stoppable coroutines for Unity

These are intended for managing stuff like async tasks with timeouts, async tasks which must be explicitly cancelled once started etc.

Project contains the following classes & a rudimentary test harness to test them as a unity scene.

**NOTE** *These classes rely on Unity's monobehaviour.StartCoroutine / StopCoroutine but could easliy be modified to work with any IEnumerator based coroutine system*


## InstantStopCoroutine

* wraps an IEnumerator which can be stopped instantly by calling Stop()

## StoppableCoroutine

* wraps a "running" and a separate "stopping" IEnumerator 
* will enter a 'stopping' state & enumerate the 'stopping' one if the 'running' IEnumerator wasn't finished when stopped



## LICENSING

*Released under MIT license: https://opensource.org/licenses/MIT*

> Copyright 2021 Alex Darby
> 
> Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
> 
> The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
> 
> THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.