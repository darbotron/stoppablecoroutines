using System;
using System.Collections;
using System.Reflection;
using UnityEngine;


public class StartAndStopCoroutines : MonoBehaviour
{
    // decorator interface to add debugging info to the stoppable coroutines for the test harness
    interface IEnumeratorDebug
    {
        string Name           { get; }
        int    IterationCount { get; }
    }

    // debug proxy to InstantStopCoroutine 
    class DebugInstantStopCoroutine : InstantStopCoroutine, IEnumerator, IEnumeratorDebug
    {
        public int    IterationCount => m_iterationCount;
        public string Name           { get; private set; }

        // InstantStopCoroutine Interface
        bool IEnumerator.MoveNext()
        {
            if( base.MoveNext() )
            {
                m_iterationCount++;
                return true;
            }
            return false;
        }
        // InstantStopCoroutine Interface

        public static DebugInstantStopCoroutine StartCoroutineOn( MonoBehaviour coroutineMonobehaviour, IEnumerator managedEnumerator, string debugName ) => new DebugInstantStopCoroutine( coroutineMonobehaviour, managedEnumerator, null, debugName ); 
        public static DebugInstantStopCoroutine StartCoroutineOn( MonoBehaviour coroutineMonobehaviour, IEnumerator managedEnumerator, Action cbOnManuallyStopped, string debugName ) => new DebugInstantStopCoroutine( coroutineMonobehaviour, managedEnumerator, cbOnManuallyStopped, debugName );
        
        private int m_iterationCount = 0;

        private DebugInstantStopCoroutine( MonoBehaviour coroutineMonobehaviour, IEnumerator managedEnumerator, Action cbOnManuallyStopped, string debugName ) : base( coroutineMonobehaviour, managedEnumerator, cbOnManuallyStopped )
        {
            Name = debugName;
        }
    }

    // debug proxy to StoppableCoroutine 
    class DebugStoppableCoroutine : StoppableCoroutine, IEnumerator, IEnumeratorDebug
    {
        public int    IterationCount => m_iterationCount;
        public string Name           { get; private set; }

        // InstantStopCoroutine Interface
        bool IEnumerator.MoveNext()
        {
            if( base.MoveNext() )
            {
                m_iterationCount++;
                return true;
            }
            return false;
        }
        // InstantStopCoroutine Interface

        public static DebugStoppableCoroutine StartCoroutineOn( MonoBehaviour coroutineMonobehaviour, IEnumerator managedEnumeratorRunning, IEnumerator managedEnumeratorStopping, string debugName ) => new DebugStoppableCoroutine( coroutineMonobehaviour, managedEnumeratorRunning, managedEnumeratorStopping, debugName ); 

        private int m_iterationCount = 0;

        private DebugStoppableCoroutine( MonoBehaviour coroutineMonobehaviour, IEnumerator managedEnumeratorRunning, IEnumerator managedEnumeratorStopping, string debugName ) : base( coroutineMonobehaviour, managedEnumeratorRunning, managedEnumeratorStopping )
        {
            Name = debugName;
        }
    }

    // monobehaviour code
    const int k_XFrames = 3;

    IEnumerator          m_manualCoroutineForLogic = null;
    IStoppableEnumerator m_stoppableTester         = null;

    void Start()
    {
        m_manualCoroutineForLogic = CoOverallLogic();
    }

    void Update()
    {
        var stoppableStateName = "null";
        var stoppableName      = "null";
        int numIterations      = -1;
        if( null != m_stoppableTester )
        {
            stoppableStateName = ( m_stoppableTester.IsRunning() ? "running" : ( m_stoppableTester.IsStopping() ? "stopping" : ( m_stoppableTester.HasStopped() ? "stopped" : "error!" ) ) );
            stoppableName      = ( m_stoppableTester is IEnumeratorDebug ? ((IEnumeratorDebug) m_stoppableTester ).Name : "No name" );
            numIterations      = ( m_stoppableTester is IEnumeratorDebug ? ((IEnumeratorDebug) m_stoppableTester ).IterationCount : -1 ); 
        }
        Debug.Log( $"[{Time.frameCount}] {GetType().Name}.{MethodBase.GetCurrentMethod().Name} - {stoppableName}:{stoppableStateName} (iterations:{numIterations})" );

        m_manualCoroutineForLogic.MoveNext();
    }


    IEnumerator CoOverallLogic()
    {
        m_stoppableTester = DebugInstantStopCoroutine.StartCoroutineOn( this, CoRunXTimes(), nameof( CoRunXTimes ) );
        Debug.Assert( ((IEnumeratorDebug) m_stoppableTester ).IterationCount == 1 );
        Debug.Assert( m_stoppableTester.IsRunning() ); 
        Debug.Assert( ! m_stoppableTester.IsStopping() );
        Debug.Assert( ! m_stoppableTester.HasStopped() );

        while( m_stoppableTester.IsRunning() )
        {
            Debug.Assert( m_stoppableTester.IsRunning() ); 
            Debug.Assert( ! m_stoppableTester.IsStopping() );
            Debug.Assert( ! m_stoppableTester.HasStopped() );
            yield return null;
        }

        Debug.Assert( ((IEnumeratorDebug) m_stoppableTester ).IterationCount == k_XFrames );
        Debug.Assert( ! m_stoppableTester.IsRunning() ); 
        Debug.Assert( ! m_stoppableTester.IsStopping() ); 
        Debug.Assert( m_stoppableTester.HasStopped() ); 

        //////////////////////////////////////////////////
        {
            var manuallyStoppedCallbackWasCalled = false;
            m_stoppableTester = DebugInstantStopCoroutine.StartCoroutineOn( this, CoRunXTimes(), () => manuallyStoppedCallbackWasCalled = true, nameof( CoRunXTimes ) );
            Debug.Assert( ((IEnumeratorDebug) m_stoppableTester ).IterationCount == 1 );
            Debug.Assert( m_stoppableTester.IsRunning() ); 
            Debug.Assert( ! m_stoppableTester.IsStopping() );
            Debug.Assert( ! m_stoppableTester.HasStopped() );
            Debug.Assert( ! manuallyStoppedCallbackWasCalled );

            while( m_stoppableTester.IsRunning() )
            {
                Debug.Assert( m_stoppableTester.IsRunning() ); 
                Debug.Assert( ! m_stoppableTester.IsStopping() );
                Debug.Assert( ! m_stoppableTester.HasStopped() );
                Debug.Assert( ! manuallyStoppedCallbackWasCalled );
                yield return null;
            }

            Debug.Assert( ((IEnumeratorDebug) m_stoppableTester ).IterationCount == k_XFrames );
            Debug.Assert( ! m_stoppableTester.IsRunning() ); 
            Debug.Assert( ! m_stoppableTester.IsStopping() ); 
            Debug.Assert( m_stoppableTester.HasStopped() ); 
            Debug.Assert( ! manuallyStoppedCallbackWasCalled );
        }

        //////////////////////////////////////////////////
        {
            var manuallyStoppedCallbackWasCalled = false;
            m_stoppableTester = DebugInstantStopCoroutine.StartCoroutineOn( this, CoRunUntilStopped(), () => manuallyStoppedCallbackWasCalled = true, nameof( CoRunUntilStopped ) );        
            Debug.Assert( ((IEnumeratorDebug) m_stoppableTester ).IterationCount == 1 );
            Debug.Assert( m_stoppableTester.IsRunning() ); 
            Debug.Assert( ! m_stoppableTester.IsStopping() );
            Debug.Assert( ! m_stoppableTester.HasStopped() );
            Debug.Assert( ! manuallyStoppedCallbackWasCalled );

            {
                int iLoopsLeft = k_XFrames - 1; // StartCouroutine causes a single iteration
                do 
                {
                    Debug.Assert( m_stoppableTester.IsRunning() ); 
                    Debug.Assert( ! m_stoppableTester.IsStopping() );
                    Debug.Assert( ! m_stoppableTester.HasStopped() );
                    Debug.Assert( ! manuallyStoppedCallbackWasCalled );
                    yield return null;
                }
                while( iLoopsLeft-- > 0 );

                Debug.Assert( ((IEnumeratorDebug) m_stoppableTester ).IterationCount == ( k_XFrames ), $"NumIterations: {((IEnumeratorDebug) m_stoppableTester ).IterationCount} - expected {( k_XFrames + 1 )}" );
            }

            Debug.Assert( m_stoppableTester.IsRunning() ); 
            Debug.Assert( ! m_stoppableTester.IsStopping() );
            Debug.Assert( ! m_stoppableTester.HasStopped() );
            Debug.Assert( ! manuallyStoppedCallbackWasCalled );
            m_stoppableTester.Stop();

            Debug.Assert( ! m_stoppableTester.IsRunning() ); 
            Debug.Assert( ! m_stoppableTester.IsStopping() ); 
            Debug.Assert( m_stoppableTester.HasStopped() );
            Debug.Assert( manuallyStoppedCallbackWasCalled );
        }

        //////////////////////////////////////////////////    
        m_stoppableTester = DebugStoppableCoroutine.StartCoroutineOn( this, CoRunWithStopping_Running(), CoRunWithStopping_Stopping(), nameof( CoRunWithStopping_Running ) );        
        Debug.Assert( ((IEnumeratorDebug) m_stoppableTester ).IterationCount == 1 );
        Debug.Assert( m_stoppableTester.IsRunning() ); 
        Debug.Assert( ! m_stoppableTester.IsStopping() );
        Debug.Assert( ! m_stoppableTester.HasStopped() );

        {
            int iLoopsLeft = k_XFrames - 1; // StartCouroutine causes a single iteration
            do
            {
                Debug.Assert( m_stoppableTester.IsRunning() ); 
                Debug.Assert( ! m_stoppableTester.IsStopping() );
                Debug.Assert( ! m_stoppableTester.HasStopped() );
                yield return null;
            }
            while( iLoopsLeft-- > 0 );
            Debug.Assert( ((IEnumeratorDebug) m_stoppableTester ).IterationCount == ( k_XFrames ) );
        }

        Debug.Assert( m_stoppableTester.IsRunning() ); 
        Debug.Assert( ! m_stoppableTester.IsStopping() );
        Debug.Assert( ! m_stoppableTester.HasStopped() );
        m_stoppableTester.Stop();

        Debug.Assert( !m_stoppableTester.IsRunning() ); 
        Debug.Assert( m_stoppableTester.IsStopping() );
        Debug.Assert( ! m_stoppableTester.HasStopped() );

        while( m_stoppableTester.IsStopping() )
        {
            Debug.Assert( !m_stoppableTester.IsRunning() ); 
            Debug.Assert( m_stoppableTester.IsStopping() );
            Debug.Assert( ! m_stoppableTester.HasStopped() );
            yield return null;
        }

        Debug.Assert( !m_stoppableTester.IsRunning() ); 
        Debug.Assert( !m_stoppableTester.IsStopping() );
        Debug.Assert( m_stoppableTester.HasStopped() );

        // halt on error in the editor!
        Debug.Break();
    }

    IEnumerator CoRunXTimes()
    {
        int iLoopsLeft = k_XFrames;
        do
        {
            yield return null;
        }
        while( --iLoopsLeft > 0 );
    }

    IEnumerator CoRunUntilStopped()
    {
        var keepRunning = true;

        while( keepRunning )
        {
            yield return null;
        }
    }

    IEnumerator CoRunWithStopping_Running()
    {
        var keepRunning = true;

        while( keepRunning )
        {
            yield return null;
        }
    }

    IEnumerator CoRunWithStopping_Stopping()
    {
        int iLoopsLeft = k_XFrames;
        do
        {
            Debug.Log( $"CoRunWithStopping_Stopping - iLoopsLeft: {iLoopsLeft}" );
            yield return null;
        }
        while( --iLoopsLeft > 0 );
    }

}
