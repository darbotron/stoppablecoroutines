using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Assert
{
	public static void Fatal( bool conditionToTestForTrue, string assertMessage )
	{
		Debug.Assert( conditionToTestForTrue, assertMessage );
	}
} 