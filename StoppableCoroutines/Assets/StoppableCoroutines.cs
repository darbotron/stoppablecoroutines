using System;
using System.Collections;
using UnityEngine;


    //
    // interface which extends IEnumerator to add a bunch of useful stuff for when using one as a coroutine in Unity
    // 
    public interface IStoppableEnumerator : IEnumerator
    {
        // IEnumerator: void   Reset();   - MS docs say it's officially part of the IEnumerator spec to throw new NotSupportedException() if this operation isn't supported        
        // IEnumerator: object Current { get; }  
        // IEnumerator: bool   MoveNext();               
        bool   IsRunning();     // returns true from construction til Stop() or ForceStop() are called, or IEnumerator MoveNext has returned false 
        void   Stop();          // attempts to stop the enumeration, if IsStopping() returns true after this call additional calls to MoveNext() are required to complete the enumeration             
        bool   IsStopping();    // returns true after Stop() if requires additional calls to MoveNext() required to complete the enumeration        
        bool   HasStopped();    // returns true when enumeration has stopped (i.e. if this is true MoveNext() must return false when called)  
        void   ForceStop();     // tries to instantly stop the IEnumerator - allowed to throw System.NotSupportedException
    }


    //
    // Wraps a single IEnumerator / MonoBehaviour pairing
    //
    // The IEnumerator is run as a coroutine on the paired MonoBehaviour at creation using MonoBehaviour.StartCoroutine()
    //
    // Provides the following super useful extensions to the basic coroutine stuff:
    //
    // * call InstantStopCoroutine.StartCoroutineOn() to create an instance and start it as a coroutine 
    // * check if the IEnumerator is running by calling IsRunning()
    // * check if the IEnumerator has finished by calling HasStopped()
    // * stop at any time by calling Stop() (calls MonoBehaviour.StopCoroutine() on the paired MonoBehaviour) - this instantly transitions from IsRunning() to HasStopped()
    // * call a callback Action if the object is manually stopped  
    // * asserts on operations done in invalid states
    // * DOES NOT support IEnumerator.Reset() - throws NotSupportedException if called 
    //
    public class InstantStopCoroutine : IStoppableEnumerator
    {
        //////////////////////////////////////////////////////////////////////////
        #region IEnumerator interface


        public void Reset()
        {
            throw new NotSupportedException( "InstantStopCoroutine doesn't support Reset()" ); 
        }

        public object Current => m_managedEnumerator.Current;

        public bool MoveNext()
        {
            if( HasStopped() )
            {
                return false;
            }

            if( m_managedEnumerator.MoveNext() )
            {
                return true;
            }

            CurrentState = State.Stopped;
            return false;
        }


        #endregion IEnumerator interface
        //////////////////////////////////////////////////////////////////////////
        #region IStoppableEnumerator interface


        public bool IsRunning()  => ( State.Running  == CurrentState );
        public bool IsStopping() => false; // stops instantly        
        public bool HasStopped() => ( State.Stopped  == CurrentState );
        
        public void Stop()
        {
            Assert.Fatal( ( State.Stopped != CurrentState ), $"can't stop again, already stopped" );

            switch( CurrentState )
            {
            case State.Running:
                m_coroutineMonobehaviour.StopCoroutine( this );
                CurrentState = State.Stopped;
                m_cbOnWasManuallyStopped?.Invoke();
                break;
            }
        }

        public void ForceStop() => Stop();
        

        #endregion IStoppableEnumerator interface
        //////////////////////////////////////////////////////////////////////////
        #region public


        //------------------------------------------------------------------------        
        public static InstantStopCoroutine StartCoroutineOn( MonoBehaviour runningMonobehaviour, IEnumerator enumeratorToRun, Action cbOnWasManuallyStopped ) => new InstantStopCoroutine( runningMonobehaviour, enumeratorToRun, cbOnWasManuallyStopped );

        //------------------------------------------------------------------------        
        public static InstantStopCoroutine StartCoroutineOn( MonoBehaviour runningMonobehaviour, IEnumerator enumeratorToRun ) => new InstantStopCoroutine( runningMonobehaviour, enumeratorToRun, null );


        #endregion public
        //////////////////////////////////////////////////////////////////////////
        #region private


        private enum State
        {
            Running,
            Stopped
        }

        private State CurrentState { get; set; }

        //------------------------------------------------------------------------
        private readonly MonoBehaviour m_coroutineMonobehaviour = null;
        private readonly IEnumerator   m_managedEnumerator      = null;
        private readonly Action        m_cbOnWasManuallyStopped = null;


        //------------------------------------------------------------------------
        // note: protected to allow deriving classes 
        //------------------------------------------------------------------------
        protected InstantStopCoroutine( MonoBehaviour coroutineMonobehaviour, IEnumerator managedEnumerator, Action cbOnWasManuallyStopped )
        {
            Assert.Fatal( ( null != managedEnumerator ), $"{nameof(managedEnumerator)} must be a valid IEnumerator" );

            m_coroutineMonobehaviour = coroutineMonobehaviour;
            m_managedEnumerator      = managedEnumerator;
            m_cbOnWasManuallyStopped = cbOnWasManuallyStopped;

            CurrentState = State.Running;
            m_coroutineMonobehaviour.StartCoroutine( this );
        }


        #endregion private
        //////////////////////////////////////////////////////////////////////////
    }


    //
    // Wraps a pair of IEnumerators and associates with a MonoBehaviour
    //
    // Two IEnumerators are needed for this class, one enumerated to 'run' the coroutine, and another enumerated used for 'stopping' it
    //
    // The 'run' IEnumerator is run on the paired MonoBehaviour at creation using MonoBehaviour.StartCoroutine()
    // IsRunning() will now return true
    //
    // If Stop() is called before the 'run' enumerator has completed, then the 'stopping' enumerator will be enumerated
    // by IEnumerator.MoveNext() (which is what Unity uses to update Coroutines)
    //
    //
    // * call StoppableCoroutine.StartCoroutineOn() to create an instance and start it as a coroutine
    // * check if the IEnumerator is running by calling IsRunning() - if this is true then the 'run' IEnumerator is being enumerated when IEnumerator.MoveNext() is called 
    // * stop at any time by calling Stop() (calls MonoBehaviour.StopCoroutine() on the paired MonoBehaviour) - this instantly transitions from IsRunning() to HasStopped()
    // * check if the IEnumerator is stopping by calling IsStopping() - if this is true then the 'stopping' IEnumerator is being enumerated when IEnumerator.MoveNext() is called 
    // * check if the IEnumerator has finished by calling HasStopped() - if this is true then either the 'run' or 'stopping' enumerators enumerated to completion 
    // * can force an instant transition from IsRunning() to HasStopped() with ForceStop() - however this is not guaranteed to be safe, will depend on what the IEnumerators are actually doing 
    // * asserts on operations done in invalid states
    // * DOES NOT support IEnumerator.Reset() - throws NotSupportedException if called 
    //
    public class StoppableCoroutine : IStoppableEnumerator
    {
        //////////////////////////////////////////////////////////////////////////
        #region IEnumerator interface

        
        public void Reset()
        {
            throw new NotSupportedException( "StoppableCoroutine doesn't support Reset()" ); 
        }

        public object Current => CurrentEnumerator.Current;

        public bool MoveNext()
        {
            if( HasStopped() )
            {
                return false;
            }

            if( CurrentEnumerator.MoveNext() )
            {
                return true; 
            }

            CurrentState = State.Stopped;
            return false;
        }


        #endregion IEnumerator interface
        //////////////////////////////////////////////////////////////////////////
        #region IStoppableEnumerator interface

        
        public bool IsRunning()  => ( State.Running  == CurrentState );
        public bool IsStopping() => ( State.Stopping == CurrentState );        
        public bool HasStopped() => ( State.Stopped  == CurrentState );
        
        public void Stop()
        {
            Assert.Fatal( ( State.Stopped  != CurrentState ), $"can't stop again, already stopped" );
            Assert.Fatal( ( State.Stopping != CurrentState ), $"can't stop again, already stopping" );

            switch( CurrentState )
            {
            case State.Running:
                // swap to the stopping enumerator - MoveNext must then be called til it returns false
                CurrentEnumerator = m_managedEnumeratorStopping;
                CurrentState      = State.Stopping;
                break;
            }
        }

        public void ForceStop()
        {
            m_coroutineMonobehaviour.StopCoroutine( this );
            CurrentState = State.Stopped;
        }

        
        #endregion IStoppableEnumerator interface
        //////////////////////////////////////////////////////////////////////////
        #region public

        
        public static StoppableCoroutine StartCoroutineOn( MonoBehaviour runningMonobehaviour, IEnumerator enumeratorToRun, IEnumerator enumeratorToStop ) => new StoppableCoroutine( runningMonobehaviour, enumeratorToRun, enumeratorToStop );


        #endregion public
        //////////////////////////////////////////////////////////////////////////
        #region private

        
        private enum State
        {
            Running,
            Stopping,
            Stopped
        }

        private State CurrentState { get; set; }

        private readonly MonoBehaviour m_coroutineMonobehaviour    = null;
        private readonly IEnumerator   m_managedEnumeratorRunning  = null;
        private readonly IEnumerator   m_managedEnumeratorStopping = null;

        private          IEnumerator   CurrentEnumerator {get; set;}


        protected StoppableCoroutine( MonoBehaviour coroutineMonobehaviour, IEnumerator managedEnumeratorRunning, IEnumerator managedEnumeratorStopping )
        {
            Assert.Fatal( ( null != managedEnumeratorRunning ),     $"{nameof(managedEnumeratorRunning)} must be a valid IEnumerator" );
            Assert.Fatal( ( null != managedEnumeratorStopping ),    $"{nameof(managedEnumeratorStopping)} must be a valid IEnumerator" );

            m_coroutineMonobehaviour      = coroutineMonobehaviour; 
            m_managedEnumeratorRunning  = managedEnumeratorRunning;
            m_managedEnumeratorStopping = managedEnumeratorStopping;

            CurrentState                = State.Running;
            CurrentEnumerator           = m_managedEnumeratorRunning;
            m_coroutineMonobehaviour.StartCoroutine( this );
        }

        
        #endregion private
        //////////////////////////////////////////////////////////////////////////
    }
