using System.Collections;
using UnityEngine;

public class ChainedCoroutines : MonoBehaviour
{
    void Start()
    {
        StartCoroutine( CoRoutineOne() );         
    }

    IEnumerator CoRoutineOne()
    {
        int i = 0;
        
        Debug.Log( $"[{Time.frameCount}] CoRoutineOne: {i++}" );
        yield return CoRoutineTwo(); 

        Debug.Log( $"[{Time.frameCount}] CoRoutineOne: {i++}" );
        yield return CoRoutineThree(); 

        Debug.Log( $"[{Time.frameCount}] CoRoutineOne: {i++} - end" );
    }

    IEnumerator CoRoutineTwo()
    {
        int i = 0;
        
        Debug.Log( $"[{Time.frameCount}] CoRoutineTwo: {i++}" );
        yield return null;

        Debug.Log( $"[{Time.frameCount}] CoRoutineTwo: {i++}" );
    }

    IEnumerator CoRoutineThree()
    {
        int i = 0;
        
        Debug.Log( $"[{Time.frameCount}] CoRoutineThree: {i++}" );
        yield return null;

        Debug.Log( $"[{Time.frameCount}] CoRoutineThree: {i++}" );
        yield return null;

        Debug.Log( $"[{Time.frameCount}] CoRoutineThree: {i++}" );
        yield return CoRoutineFour();
        
        Debug.Log( $"[{Time.frameCount}] CoRoutineThree: {i++}" );
        yield return null;

        Debug.Log( $"[{Time.frameCount}] CoRoutineThree: {i++} - end" );
    }

    IEnumerator CoRoutineFour()
    {
        int i = 0;
        
        Debug.Log( $"[{Time.frameCount}] CoRoutineFour: {i++}" );
        yield return null;
        
        Debug.Log( $"[{Time.frameCount}] CoRoutineFour: {i++}" );
        yield return null;
        
        Debug.Log( $"[{Time.frameCount}] CoRoutineFour: {i++}" );
        yield return null;

        Debug.Log( $"[{Time.frameCount}] CoRoutineFour: {i++} - end" );
    }
}
